# README #

FLUTWEET-ID merupakan sebuah sistem pemantau penyebaran ILI berdasarkan data tweet berbahasa Indonesia. Pada penelitian ini, tweet yang digunakan yaitu tweet yang mengandung kata kunci yang berkaitan dengan flu. Teknik yang digunakan yaitu klasifikasi dengan algoritma KNN (K-Nearest Neighbors).

### Keterangan Repository ###

Repository terdiri dari 7 packaged, yaitu :
1. corpus : terdapat class untuk memproses json ke database
2. form : paket yang berisi form untuk interkasi dengan user.
3. klasifikasi : Paket yang berisi kelas-kelas yang digunakan pada proses klasifikasi, yaitu perhitungan cosine similarity dan kNN. 
4. main : Paket yang berisi kelas yang berinteraksi langsung dengan user.
5. pembobotan : Paket yang berisi kelas yang digunakan pada proses pembobotan, yaitu perhitungan TFIDF.
6. preprocessing : paket yang berisi kelas yg digunakan pada proses preprocessing
7. visualisasi : paket yang berisi kelas untuk visualisasi informasi


*NOTE*
Modul telah selesai hingga klasifikasi

ON PROGRESS : visualisasi

### Cara Menjalankan ###

Repository ini merupakan sebuah NetbeansProject, cara menjalankannya yaitu dengan open project di netbeans.
Aplikasi terdiri dari 2 jenis berdasarkan pengguna, untuk admin dan untuk user biasa.
Untuk menjalankan aplikasi admin buka package Main dan run Admin.java, untuk user buka package Main dan run User.java

inputan yang dapat diunduh di link berikut :
https://www.dropbox.com/sh/rmrnr66v8vayaak/AABBEswzHRVWf9P2OnpncZAJa?dl=0

Repository ini menggunakan library yang hanya digunakan pada packaged "corpus" (parse Json ke DB). Library dapat diunduh di alamat : https://www.dropbox.com/s/4oj7fapinqdx2wp/library.rar?dl=0
namun, aplikasi dapat berjalan tanpa library tersebut. cukup dengan menghapus packaged "corpus" pada repository