/**
 *
 * @author Indana Zulfa
 */
package Corpus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import org.apache.commons.io.FileUtils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonToDB {

    static final String driver = "com.mysql.jdbc.Driver";
    static final String url = "jdbc:mysql://127.0.0.1:3306/tweet";
    static final String user = "root";
    static final String pass = "";

    public static void main(String[] args){
        File folder = new File("F:\\output");
        Connection connect = null;
        PreparedStatement pstmt = null;
        final int batchSize = 5000;
        int count = 0;

        try {
            connect = DriverManager.getConnection(url, user, pass);
            connect.setAutoCommit(false);
            Class.forName("com.mysql.jdbc.Driver");

            //Open a connection
            String sql = "INSERT INTO tes(tanggal,tweet,lokasi,uname) VALUES(?,?,?,?)";
            pstmt = connect.prepareStatement(sql);
            for (File file : folder.listFiles()) {
                if (file.isFile() && file.getName().endsWith(".json")) {
                    String content = FileUtils.readFileToString(file);
                    String jsonStr = "{\"no\":[" + content.replace("}", "},") + "]}";

                    JSONParser jsonParser = new JSONParser();
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonStr);
                    JSONArray no = (JSONArray) jsonObject.get("no");
                    Iterator k = no.iterator();

                    //Register JDBC driver
                    while (k.hasNext()) {
                        JSONObject jObj = (JSONObject) k.next();
                        String tweet = (String) jObj.get("text");
                        tweet = tweet.replace("\n", " ").replace("|", "");
                        String created_at = (String) jObj.get("created_at");
                        SimpleDateFormat sdt = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy");
                        java.util.Date dt = sdt.parse(created_at);
                        SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        created_at = d.format(dt);

                        String bahasa = (String) jObj.get("lang");

                        // untuk json didalam struktur user
                        JSONObject structure = (JSONObject) jObj.get("user");
                        String lokasi = (String) structure.get("location");
                        lokasi = lokasi.replace("\n", " ");
                        String uname = (String) structure.get("screen_name");

                        //input ke database

                        if ("in".equals(bahasa)) {
                            pstmt.setString(1, created_at);
                            pstmt.setString(2, tweet);
                            pstmt.setString(3, lokasi);
                            pstmt.setString(4, uname);
                            pstmt.addBatch();

                            if (++count % batchSize == 0) {
                                System.out.println("batch " + count);
                                pstmt.executeBatch();
                                connect.commit();
                            }
                        }
                    }
                    pstmt.executeBatch();
                    connect.commit();
                }
            }
            System.out.println("complete! cek database...");
        } catch (FileNotFoundException f) {
            f.printStackTrace();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ParseException p) {
            p.printStackTrace();
        } catch (SQLException s) {
            s.printStackTrace();
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        } catch (java.text.ParseException ex) {
            Logger.getLogger(JsonToDB.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                if (pstmt != null) {
                    connect.close();
                }
            } catch (SQLException se) {
            }
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
