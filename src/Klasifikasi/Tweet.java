/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Klasifikasi;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author indana zulfa
 */
public class Tweet {
    public ArrayList<Double> tfidf;
    public String tweet;
    public String tanggal;
    public String lokasi;
    public String label;
    public HashMap<String,Double> tweet_tfidf;
    
    public Tweet(ArrayList<Double> tfidf, String tweet, String tanggal, String lokasi, String label, HashMap<String,Double> tweet_tfidf){
        this.tfidf = tfidf;
        this.tweet = tweet;
        this.tanggal = tanggal;
        this.lokasi = lokasi;
        this.label = label;
        this.tweet_tfidf = tweet_tfidf;
    }
    
    public String getTweet(){
        return this.tweet;
    }
    
    public HashMap<String, Double> getTweetTFIDF(){
        return this.tweet_tfidf;
    }
    
    public String getLabel(){
        return this.label;
    }
}
