/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Klasifikasi;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

/**
 *
 * @author Indana Zulfa
 */
public class KNN {

    String input = null;
    String output = null;
    int k = 0;
    Vector<HashMap<String, Double>> v_tfidf;
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/skripsi";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "";

    public KNN(String i, String o, int k) {
        this.input = i;
        this.output = o;
        this.k = k;
    }

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        String input = "D:\\data tweet\\tes_tfidf.txt";
        String output = "D:\\data tweet\\tes_class.txt";
        int k = 5;

        KNN c = new KNN(input, output, k);
        c.process();
    }

    public void process() throws FileNotFoundException, IOException, ClassNotFoundException {
        Connection conn = null;
        Statement stmt = null;
        File in = new File(input);
        File out = new File(output);
        int k = this.k;

        //read file
        FileReader reader = new FileReader(in);
        BufferedReader br = new BufferedReader(reader);
        FileWriter writer = new FileWriter(out);

        v_tfidf = new Vector<>(); //variabel untuk menampung vektor tfidf

        ArrayList<HashMap<Integer, Tweet>> a_data_train = new ArrayList<>(); //variabel untuk menampung data train
        ArrayList<HashMap<Integer, Tweet>> a_data_test = new ArrayList<>();  //variabel untuk menampung data test
        HashMap<Integer, Tweet> d_train = new HashMap<>();  //variabel untuk menampung data train
        HashMap<Integer, Tweet> d_test = new HashMap<>();  //variabel untuk menampung data test
        HashMap<Integer, Tweet> all = new HashMap<>();  //variabel untuk menampung semua data
        ArrayList<Double> tfidf = new ArrayList<>(); //variabel untuk menampung nilai tfidf
        HashMap<String, Double> tweet_tfidf = new HashMap<>(); //variabel untuk menampung nilai tfidf setiap kata

        int max = 0;
        String line = null;
        int jumlah = 0;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            PreparedStatement pstmt = null;
            String del = "DROP TABLE IF EXISTS TWEET";
            stmt.executeUpdate(del);
            String sql = "CREATE TABLE TWEET "
                    + "(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, "
                    + " tweet TEXT, "
                    + " tanggal DATETIME, "
                    + " lokasi VARCHAR(50), "
                    + " label VARCHAR(50)"
                    + " )";

            stmt.executeUpdate(sql);
            String insert = "INSERT INTO TWEET(tweet, tanggal, lokasi, label) VALUES(?,?,?,?)";
            pstmt = conn.prepareStatement(insert);
            while ((line = br.readLine()) != null) {
                if (line.contains("||")) {
                    String[] tweet = line.split("[\\x7C][\\x7C]");
                    String tweetTFIDF = tweet[5];
                    StringTokenizer st = new StringTokenizer(tweetTFIDF, ";\t\n\r\f=");
                    int c = st.countTokens() / 2;
                    tweet_tfidf = new HashMap<String, Double>();

                    for (int j = 0; j < c; j++) {
                        tweet_tfidf.put(st.nextToken(), Double.parseDouble(st.nextToken())); //input nilai tfidf per kata, pada setiap tweet
                    }
                    if (c > 0) {
                        max = Math.max(max, tweet_tfidf.size());
                    }
                    v_tfidf.addElement(tweet_tfidf);
                    tfidf = getValueOfHashMap(tweet_tfidf);
                    all.put(jumlah, new Tweet(tfidf, tweet[0], tweet[2], tweet[3], tweet[4], tweet_tfidf));
                    jumlah++;
                    if(tweet[4].trim().equalsIgnoreCase("flu") || tweet[4].trim().equalsIgnoreCase("bukan_flu")){
                    pstmt.setString(1, tweet[0]);
                    pstmt.setString(2, tweet[2]);
                    pstmt.setString(3, tweet[3]);
                    pstmt.setString(4, tweet[4]);
                    pstmt.executeUpdate();
                    //conn.commit();
                    }
                }
            }
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException se) {
            }// do nothing
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        int kfold = 10;
        int sizePerFold = jumlah / kfold;
        int batas_awal = 0;
        int batas_akhir = sizePerFold;
        int i_test = 0;
        int i_train = 0;
        int it1 = 0;
        int it2 = 0;

        //10 folds bagi data
        for (int i = 0; i < kfold; i++) {
            int status = 0;
            int iterasi = 0;
            while (status == 0) {
                Iterator it = all.entrySet().iterator();
                while (it.hasNext()) {
                    if (iterasi >= batas_awal && iterasi < batas_akhir) {
                        d_test = new HashMap<>();
                        Map.Entry pairs = (Map.Entry) it.next();
                        d_test.put(it1, (Tweet) pairs.getValue());
                        it1++;
                        a_data_test.add(i_test, d_test);
                        i_test++;
                    } else {
                        d_train = new HashMap<>();
                        Map.Entry pairs = (Map.Entry) it.next();
                        d_train.put(it2, (Tweet) pairs.getValue());
                        it2++;
                        a_data_train.add(i_train, d_train);
                        i_train++;
                    }
                    if (iterasi >= jumlah - 1) {
                        status = 1;
                    }
                    iterasi++;
                }
            }
            if (i == kfold - 1) {
                batas_awal = batas_akhir;
                batas_akhir = jumlah;
            } else {
                batas_awal = batas_akhir;
                batas_akhir += sizePerFold;
            }
        }

        //10 folds process KNN
        ArrayList<Double> cosine = new ArrayList<>();
        ArrayList<String> label = new ArrayList<>();

        int batchSize = jumlah - sizePerFold;
        int batchSize2 = sizePerFold;
        int start = 0;
        int start2 = 0;
        int end = batchSize;
        int end2 = batchSize2;
        int count = a_data_train.size() / batchSize;
        int reminder = a_data_train.size() % batchSize;
        int reminder2 = a_data_test.size() % batchSize2;
        int counter = 0;
        int counter2 = 0;

        double accuracy = 0;
        double recall = 0;
        double precision = 0;
        double fmeasure = 0;
        double acc = 0;
        double rec = 0;
        double prec = 0;
        double f1 = 0;

        writer.write("=== Run Information ===\r\n\r\n");
        writer.write("Instances : " + jumlah + "\r\n");
        writer.write("Test Mode : " + kfold + "-fold cross-validation\r\n\r\n");
        writer.write("=== Classifier Model ===\r\n\r\n");
        writer.write("kNN Classifier\r\nk : " + k + "\r\n\r\n");
        writer.write("=== Result per Fold ===\r\n\r\n");
        for (int i = 0; i < count; i++) {
            writer.write("fold " + (i + 1) + " : ");
            System.out.println("fold " + (i + 1));
            Map<String, Integer> result = new TreeMap<String, Integer>();
            int tp = 0;
            int fp = 0;
            int tn = 0;
            int fn = 0;
            for (counter2 = start2; counter2 < end2; counter2++) {
                for (counter = start; counter < end; counter++) {
                    cosine.add(computeSim(a_data_test.get(counter2).get(counter2).tweet_tfidf, a_data_train.get(counter).get(counter).tweet_tfidf));
                    label.add(a_data_train.get(counter).get(counter).label);
                    //System.out.print("data tes : "+ a_data_test.get(counter2).get(counter2).tweet_tfidf);
                    //System.out.println("data train : "+a_data_train.get(counter).get(counter).tweet_tfidf);
                    //System.out.println(computeSim(a_data_test.get(counter2).get(counter2).tweet_tfidf, a_data_train.get(counter).get(counter).tweet_tfidf));
                    //System.out.println(a_data_train.get(counter).get(counter).label);
                }
                
                
                /*result = new TreeMap<String, Integer>();
                for (String l : label) {
                    String value = l;
                    Integer c = result.get(value);
                    if (c == null) {
                        result.put(value, new Integer(1));
                    } else {
                        result.put(value, new Integer(c + 1));
                    }
                }*/
                if (getKNN(cosine, label, k).trim().equalsIgnoreCase("bukan_sakit")) {
                    if (getKNN(cosine, label, k).trim().equalsIgnoreCase(a_data_test.get(counter2).get(counter2).label.trim())) {
                        tn++;
                    } else {
                        fp++;
                    }
                } else if (getKNN(cosine, label, k).trim().equalsIgnoreCase("sakit")) {
                    if (getKNN(cosine, label, k).trim().equalsIgnoreCase(a_data_test.get(counter2).get(counter2).label.trim())) {
                        tp++;
                    } else {
                        fn++;
                    }
                } else if (getKNN(cosine, label, k).trim().equalsIgnoreCase("bukan_flu")) {
                    if (getKNN(cosine, label, k).trim().equalsIgnoreCase(a_data_test.get(counter2).get(counter2).label.trim())) {
                        tn++;
                    } else {
                        fp++;
                    }
                } else if (getKNN(cosine, label, k).trim().equalsIgnoreCase("flu")) {
                    if (getKNN(cosine, label, k).trim().equalsIgnoreCase(a_data_test.get(counter2).get(counter2).label.trim())) {
                        tp++;
                    } else {
                        fn++;
                    }
                }
                label = new ArrayList<>();
                cosine = new ArrayList<>();
            }
            // System.out.println("train " + result);
            /*for (Map.Entry<String, Integer> entry : result.entrySet()) {
             Integer value = entry.getValue();
             String key = entry.getKey();
             if (key.trim().equalsIgnoreCase("bukan_sakit")) {
             // tn = tn+value;
             } else if (key.trim().equalsIgnoreCase("sakit")) {
             //tp = tp+value;
             }
             }*/
            accuracy = (((double) tp + (double) tn) / ((double) tp + (double) fp + (double) fn + (double) tn)) * 100;
            recall = ((double) tp / ((double) tp + (double) fn));
            precision = ((double) tp / ((double) tp + (double) fp));
            fmeasure = (2 * precision * recall) / (precision + recall);
            acc = acc + accuracy;
            rec = rec + recall;
            prec = prec + precision;
            f1 = f1 + fmeasure;
            /*System.out.print("akurasi " + accuracy + "% || ");
             System.out.print("recall " + recall + " || ");
             System.out.print("precision " + precision + " || ");
             System.out.println("fmeasure " + fmeasure + " ");*/
            writer.write("akurasi " + accuracy + "% | recall " + recall + " | precision " + precision + " | fmeasure " + fmeasure + "\r\n");
            start = start + batchSize;
            start2 = start2 + batchSize2;
            end = end + batchSize;
            end2 = end2 + batchSize2;
            
        }

        if (reminder != 0 || reminder2 != 0) {
            writer.write("fold 10 : ");
            //System.out.println("fold 10");
            Map<String, Integer> result = new TreeMap<String, Integer>();
            end = end - reminder;
            end2 = end2 + reminder2;
            int tp = 0;
            int fp = 0;
            int tn = 0;
            int fn = 0;
            for (counter2 = start2; counter2 < end2; counter2++) {
                for (counter = start; counter < end; counter++) {
                    cosine.add(computeSim(a_data_test.get(counter2).get(counter2).tweet_tfidf, a_data_train.get(counter).get(counter).tweet_tfidf));
                    label.add(a_data_train.get(counter).get(counter).label);
                }

                result = new TreeMap<String, Integer>();
                for (String l : label) {
                    String value = l;
                    Integer c = result.get(value);
                    if (c == null) {
                        result.put(value, new Integer(1));
                    } else {
                        result.put(value, new Integer(c + 1));
                    }
                }

                if (getKNN(cosine, label, k).trim().equalsIgnoreCase("bukan_sakit")) {
                    if (getKNN(cosine, label, k).trim().equalsIgnoreCase(a_data_test.get(counter2).get(counter2).label.trim())) {
                        tn++;
                    } else {
                        fp++;
                    }
                } else if (getKNN(cosine, label, k).trim().equalsIgnoreCase("sakit")) {
                    if (getKNN(cosine, label, k).trim().equalsIgnoreCase(a_data_test.get(counter2).get(counter2).label.trim())) {
                        tp++;
                    } else {
                        fn++;
                    }
                } else if (getKNN(cosine, label, k).trim().equalsIgnoreCase("bukan_flu")) {
                    if (getKNN(cosine, label, k).trim().equalsIgnoreCase(a_data_test.get(counter2).get(counter2).label.trim())) {
                        tn++;
                    } else {
                        fp++;
                    }
                } else if (getKNN(cosine, label, k).trim().equalsIgnoreCase("flu")) {
                    if (getKNN(cosine, label, k).trim().equalsIgnoreCase(a_data_test.get(counter2).get(counter2).label.trim())) {
                        tp++;
                    } else {
                        fn++;
                    }
                }
                label = new ArrayList<>();
                cosine = new ArrayList<>();
            }
            accuracy = (((double) tp + (double) tn) / ((double) tp + (double) fp + (double) fn + (double) tn)) * 100;
            recall = ((double) tp / ((double) tp + (double) fn));
            precision = ((double) tp / ((double) tp + (double) fp));
            fmeasure = (2 * precision * recall) / (precision + recall);
            acc = acc + accuracy;
            rec = rec + recall;
            prec = prec + precision;
            f1 = f1 + fmeasure;
            /*System.out.print("akurasi " + accuracy + "% || ");
             System.out.print("recall " + recall + " || ");
             System.out.print("precision " + precision + " || ");
             System.out.println("fmeasure " + fmeasure + " ");*/
            writer.write("akurasi " + accuracy + "% | recall " + recall + " | precision " + precision + " | fmeasure " + fmeasure + "\r\n\r\n");

        }
        writer.write("\r\n\r\n=== Summary ===\r\n\r\n");
        writer.write("Akurasi \t" + acc / kfold + "%" + "\r\nRecall \t" + rec / kfold + "\r\nPrecision \t" + prec / kfold + "\r\nF-Measure \t" + f1 / kfold);
        //System.out.println("rata-rata : akurasi " + acc / kfold + "%" + "|| recall " + rec/kfold + "|| precision " + prec/kfold + "|| fmeasure "+f1/kfold);
        br.close();
        writer.close();

        System.out.println("selesai knn");
    }

    public String getKNN(ArrayList<Double> cosine, ArrayList<String> label, int k) {
        String[] array = new String[3];
        int z = 0;
        ArrayIndexComparator comparator = new ArrayIndexComparator(cosine);
        Integer[] indexes = comparator.createIndexArray();
        Arrays.sort(indexes, comparator);
        //System.out.println(Arrays.toString(indexes));

        double knn[] = new double[k];
        String lbl[] = new String[k];
        int index_knn = 0;
        int n = 0;
        int n2 = 0;
        int p = 0;
        int p2 = 0;
        HashMap<Integer, String> hm = new HashMap<>();
        for (int i = 0; i < cosine.size(); i++) {
            if (cosine.size() - i <= k) {
                knn[index_knn] = cosine.get(indexes[i]);
                lbl[index_knn] = label.get(indexes[i]);
                //System.out.println(lbl[index_knn]);
                hm.put(index_knn, label.get(indexes[i]));
                index_knn++;
            }
        }
        Map<String, Integer> result = new TreeMap<String, Integer>();
        for (Map.Entry<Integer, String> entry : hm.entrySet()) {
            Integer key = entry.getKey();
            String value = entry.getValue();
            Integer count = result.get(value);

            if (count == null) {
                result.put(value, new Integer(1));
            } else {
                result.put(value, new Integer(count + 1));
            }
        }
        Map.Entry<String, Integer> maxEntry = null;

        for (Map.Entry<String, Integer> entry : result.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        //System.out.println(maxEntry.getKey());

        //System.out.println(Arrays.toString(knn));
        //System.out.println(Arrays.toString(lbl));
        return maxEntry.getKey();
    }

    public ArrayList<Double> getValueOfHashMap(HashMap<String, Double> hm) {
        ArrayList<Double> tfidf = new ArrayList<Double>();
        for (Map.Entry<String, Double> node : hm.entrySet()) {
            tfidf.add(node.getValue());
        }
        return tfidf;
    }

    public double computeSim(HashMap<String, Double> test, HashMap<String, Double> train) {
        double mul = 0, testAbs = 0, trainAbs = 0;
        Set<Map.Entry<String, Double>> testWordTFMapSet = test.entrySet();
        for (Iterator<Map.Entry<String, Double>> it = testWordTFMapSet.iterator(); it.hasNext();) {
            Map.Entry<String, Double> me = it.next();

            if (train.containsKey(me.getKey())) {
                mul += me.getValue() * train.get(me.getKey());
            }
            testAbs += me.getValue() * me.getValue();
        }
        testAbs = Math.sqrt(testAbs);

        Set<Map.Entry<String, Double>> trainTF = train.entrySet();
        for (Iterator<Map.Entry<String, Double>> it = trainTF.iterator(); it.hasNext();) {
            Map.Entry<String, Double> me = it.next();
            trainAbs += me.getValue() * me.getValue();
        }
        trainAbs = Math.sqrt(trainAbs);
        return mul / (testAbs * trainAbs);
    }

}
