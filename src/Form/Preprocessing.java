/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Form;

import Klasifikasi.KNNtest;
import Pembobotan.*;
import Preprocessing.*;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.*;
import javafx.scene.control.ComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.jdbc.JDBCCategoryDataset;
import visualisasi.frameChart;

/**
 *
 * @author Indana Zulfa
 */
public class Preprocessing extends javax.swing.JFrame {

    private File selectedFile;
    private File selectedFile2;
    private File selectedFile3;
    private File selectedFile4;
    private File selectedDirectory;
    private File selectedDirectory2;
    private Task task;
    private Task2 task2;
    private boolean error = false;

    public Preprocessing() {
        initComponents();
    }

    class Task extends SwingWorker<Void, Void> {

        @Override
        public Void doInBackground() {
            try {
                Thread.sleep(1000);
                pbLoading1.setValue(0);
                taPrepro.append("Scan File...\r\n");

                String input = selectedFile.getPath();
                String output = selectedDirectory.getPath() + "\\flutweet_prepro.txt";
                String stopwords = "D:\\TWEET\\catatan_stopwords.txt";
                String sinonim = "D:\\TWEET\\catatan_kata_sinonim.txt";
                String inputsin = output;
                String outputsin = selectedDirectory.getPath() + "\\flutweet_syn.txt";
                String inputtfidf = outputsin;
                String outputtfidf = selectedDirectory.getPath() + "\\flutweet_tfidf.txt";

                pbLoading1.setValue(30);
                taPrepro.append("PreProcessing...\r\n");
                PreproProses prepro = new PreproProses(input, output, stopwords, sinonim, outputsin, inputsin);
                prepro.process();

                pbLoading1.setValue(50);
                taPrepro.append("Remove Stopword...\r\n");
                prepro.processSW();

                pbLoading1.setValue(70);
                taPrepro.append("TFIDF...\r\n");
                TFIDF tfidf = new TFIDF();
                tfidf.hitungKata(inputtfidf);
                tfidf.hitungTFIDF(inputtfidf, outputtfidf);

                pbLoading1.setValue(100);
                taPrepro.append("FINISH...\r\n");

                bProcess1.setEnabled(true);
                bBrowse1.setEnabled(true);
                bBrowse2.setEnabled(true);
                Toolkit.getDefaultToolkit().beep();
                setCursor(null); //turn off the wait cursor
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Invalid Input File", "Error", JOptionPane.ERROR_MESSAGE);
                System.err.println(e.getMessage());
                error = true;
                bProcess1.setEnabled(false);
                bBrowse1.setEnabled(false);
                bBrowse2.setEnabled(false);
                Toolkit.getDefaultToolkit().beep();
                setCursor(null); //turn off the wait cursor
            }
            return null;
        }
    }

    class Task2 extends SwingWorker<Void, Void> {

        @Override
        public Void doInBackground() {
            try {
                pbLoading2.setValue(0);
                Thread.sleep(1000);

                taKlasifikasi.append("Scan File...\r\n");
                pbLoading2.setValue(10);
                String train1 = selectedFile2.getPath();
                String train2 = selectedFile3.getPath();
                String input = selectedFile4.getPath();
                String output = selectedDirectory2.getPath() + "\\flutweet_class.txt";
                int k = Integer.parseInt(knn.getText());

                if (k != 0 && k > 0 && k <= 10) { // if check box is checked 
                    pbLoading2.setValue(30);
                    taKlasifikasi.append("Classification with kNN...\r\n");
                    KNNtest knn = new KNNtest(train1, train2, input, output, k);
                    knn.process();

                    pbLoading2.setValue(100);
                    taKlasifikasi.append("FINISH...\r\n\r\n");

                    FileReader reader = new FileReader(output);
                    BufferedReader br = new BufferedReader(reader);
                    String line = null;
                    taKlasifikasi.append("===Classfication Result from Data Test===\r\n\r\n");
                    while ((line = br.readLine()) != null) {
                        taKlasifikasi.append(line + "\r\n");
                    }
                    try {
                        koneksi();
                        String sql = "select distinct monthname(tanggal) from tweet where label='flu'";
                        Statement stat = conn.createStatement();
                        ResultSet res = stat.executeQuery(sql);
                        while (res.next()) {
                            jComboBox1.addItem(res.getString(1));
                        }
                        String sql2 = "select distinct UPPER(lokasi) from tweet where label='flu' group by lokasi ASC";
                        Statement stat2 = conn.createStatement();
                        ResultSet res2 = stat.executeQuery(sql2);
                        while (res2.next()) {
                            jComboBox2.addItem(res2.getString(1));
                        }
                    } catch (Exception e) {
                    }
                    bProcess2.setEnabled(true);
                    Toolkit.getDefaultToolkit().beep();
                    setCursor(null); //turn off the wait cursor
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid k input", "Error", JOptionPane.ERROR_MESSAGE);
                    error = true;
                    bProcess2.setEnabled(true);
                    Toolkit.getDefaultToolkit().beep();
                    setCursor(null); //turn off the wait cursor
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Invalid Input File", "Error", JOptionPane.ERROR_MESSAGE);
                System.err.println(e.getMessage());
                error = true;
                bProcess1.setEnabled(true);
                Toolkit.getDefaultToolkit().beep();
                setCursor(null); //turn off the wait cursor
            }
            return null;
        }
    }

    public Connection conn;

    public void koneksi() throws SQLException {
        try {
            conn = null;
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/skripsi", "root", "");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ComboBox.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception es) {
            Logger.getLogger(ComboBox.class.getName()).log(Level.SEVERE, null, es);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        fileChooser = new javax.swing.JFileChooser();
        fileChooser2 = new javax.swing.JFileChooser();
        fileChooser3 = new javax.swing.JFileChooser();
        fileChooser4 = new javax.swing.JFileChooser();
        fileChooser5 = new javax.swing.JFileChooser();
        jDialog1 = new javax.swing.JDialog();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        bBrowse1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        tfFileDir = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        tfFolderDir = new javax.swing.JTextField();
        bBrowse2 = new javax.swing.JButton();
        bProcess1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        taPrepro = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        tfJumlahData = new javax.swing.JTextField();
        pbLoading1 = new javax.swing.JProgressBar();
        jLabel14 = new javax.swing.JLabel();
        lProgres = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        tfFileDir2 = new javax.swing.JTextField();
        bBrowse3 = new javax.swing.JButton();
        tfFileDir4 = new javax.swing.JTextField();
        bBrowse5 = new javax.swing.JButton();
        tfFolderDir2 = new javax.swing.JTextField();
        bBrowse6 = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        taKlasifikasi = new javax.swing.JTextArea();
        bProcess2 = new javax.swing.JButton();
        pbLoading2 = new javax.swing.JProgressBar();
        jLabel5 = new javax.swing.JLabel();
        tfFileDir3 = new javax.swing.JTextField();
        bBrowse4 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        knn = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jComboBox2 = new javax.swing.JComboBox();
        bProcess3 = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("FluTweet-ID");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        bBrowse1.setText("Browse");
        bBrowse1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bBrowse1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Choose File :");

        tfFileDir.setEditable(false);

        jLabel3.setText("Save To :");

        tfFolderDir.setEditable(false);

        bBrowse2.setText("Browse");
        bBrowse2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bBrowse2ActionPerformed(evt);
            }
        });

        bProcess1.setText("Process");
        bProcess1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bProcess1ActionPerformed(evt);
            }
        });

        taPrepro.setColumns(20);
        taPrepro.setRows(5);
        jScrollPane1.setViewportView(taPrepro);

        jLabel4.setText("Instances Number :");

        tfJumlahData.setEditable(false);

        pbLoading1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                pbLoading1PropertyChange(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel14.setText("Preprocessing");

        jLabel7.setText("Progress :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(209, 209, 209)
                        .addComponent(pbLoading1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lProgres, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel14)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(128, 128, 128))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(tfFolderDir)
                            .addComponent(tfFileDir))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(bBrowse1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(bBrowse2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(43, 43, 43))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tfJumlahData, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel3)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 504, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(245, 245, 245)
                .addComponent(bProcess1)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14)
                .addGap(31, 31, 31)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bBrowse1)
                    .addComponent(tfFileDir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfFolderDir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bBrowse2))
                .addGap(18, 18, 18)
                .addComponent(bProcess1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pbLoading1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lProgres))
                .addGap(17, 17, 17)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tfJumlahData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        jTabbedPane1.addTab("Preprocessing", jPanel1);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("K-NN Classifier");

        jLabel18.setText("Choose Data Train 1 (Class : sakit dan bukan_sakit) File :");

        jLabel23.setText("Choose Data Test File :");

        jLabel24.setText("Save To :");

        tfFileDir2.setEditable(false);

        bBrowse3.setText("Browse");
        bBrowse3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bBrowse3ActionPerformed(evt);
            }
        });

        tfFileDir4.setEditable(false);

        bBrowse5.setText("Browse");
        bBrowse5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bBrowse5ActionPerformed(evt);
            }
        });

        tfFolderDir2.setEditable(false);

        bBrowse6.setText("Browse");
        bBrowse6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bBrowse6ActionPerformed(evt);
            }
        });

        jLabel25.setText("Output :");

        taKlasifikasi.setEditable(false);
        taKlasifikasi.setColumns(20);
        taKlasifikasi.setRows(5);
        jScrollPane2.setViewportView(taKlasifikasi);

        bProcess2.setText("Process");
        bProcess2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bProcess2ActionPerformed(evt);
            }
        });

        jLabel5.setText("Choose Data Train 2 (Class : flu dan bukan_flu) File :");

        tfFileDir3.setEditable(false);

        bBrowse4.setText("Browse");
        bBrowse4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bBrowse4ActionPerformed(evt);
            }
        });

        jLabel6.setText("number of K :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfFileDir2, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addComponent(bBrowse3))
                    .addComponent(jLabel5)
                    .addComponent(jLabel23)
                    .addComponent(jLabel24)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(tfFolderDir2, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(bBrowse6))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(tfFileDir3, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfFileDir4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 447, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bBrowse5)
                            .addComponent(bBrowse4)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bProcess2)
                            .addComponent(knn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addGap(51, 51, 51)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25)
                            .addComponent(pbLoading2, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 20, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel18)
                .addGap(7, 7, 7)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfFileDir2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bBrowse3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfFileDir3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bBrowse4))
                .addGap(8, 8, 8)
                .addComponent(jLabel23)
                .addGap(4, 4, 4)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfFileDir4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bBrowse5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfFolderDir2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bBrowse6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel25)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(knn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(bProcess2))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pbLoading2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Classification", jPanel3);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel10.setText("Flu Visualization 2014");

        jLabel11.setText("Month :");

        jLabel12.setText("Province :");

        bProcess3.setText("Process");
        bProcess3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bProcess3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(bProcess3))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel10)))
                .addGap(0, 19, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addGap(32, 32, 32)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bProcess3))
                .addContainerGap(397, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Visualization", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void bBrowse1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bBrowse1ActionPerformed

        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();
            tfFileDir.setText(selectedFile.getAbsolutePath());
        } else {
            System.out.println("No Selection File");
        }

        String file = selectedFile.getPath();

        try {
            FileInputStream fstream = new FileInputStream(file);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            int nData = 0;
            try {
                while (br.readLine() != null) {
                    nData++;
                }
                tfJumlahData.setText("" + nData);
            } catch (IOException ex) {
                Logger.getLogger(Preprocessing.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Preprocessing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_bBrowse1ActionPerformed

    private void bBrowse2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bBrowse2ActionPerformed
        JFileChooser chooser;
        chooser = new JFileChooser();
        //chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //
        // disable the "All files" option.
        //
        chooser.setAcceptAllFileFilterUsed(false);
        //    
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            selectedDirectory = chooser.getSelectedFile();
            tfFolderDir.setText(selectedDirectory.getAbsolutePath());
        } else {
            System.out.println("No Selection Folder");
        }
    }//GEN-LAST:event_bBrowse2ActionPerformed

    private void bProcess1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bProcess1ActionPerformed
        // TODO add your handling code here:
        if ((selectedFile != null) && (selectedDirectory != null)) {
            bProcess1.setEnabled(false);
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            task = new Task() {
            };
            //task.addPropertyChangeListener(this);
            task.execute();
        } else {
            JOptionPane.showMessageDialog(this, "Masukkan file terlebih dahulu");
        }
    }//GEN-LAST:event_bProcess1ActionPerformed

    private void pbLoading1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_pbLoading1PropertyChange
        // TODO add your handling code here:

    }//GEN-LAST:event_pbLoading1PropertyChange

    private void bBrowse3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bBrowse3ActionPerformed
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            selectedFile2 = fileChooser.getSelectedFile();
            tfFileDir2.setText(selectedFile2.getAbsolutePath());
        } else {
            System.out.println("No Selection File");
        }
        String file = selectedFile2.getPath();
    }//GEN-LAST:event_bBrowse3ActionPerformed

    private void bBrowse4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bBrowse4ActionPerformed
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            selectedFile3 = fileChooser.getSelectedFile();
            tfFileDir3.setText(selectedFile3.getAbsolutePath());
        } else {
            System.out.println("No Selection File");
        }
        String file = selectedFile3.getPath();
    }//GEN-LAST:event_bBrowse4ActionPerformed

    private void bBrowse5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bBrowse5ActionPerformed
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            selectedFile4 = fileChooser.getSelectedFile();
            tfFileDir4.setText(selectedFile4.getAbsolutePath());
        } else {
            System.out.println("No Selection File");
        }
        String file = selectedFile4.getPath();
    }//GEN-LAST:event_bBrowse5ActionPerformed

    private void bBrowse6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bBrowse6ActionPerformed
        JFileChooser chooser;
        chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        // disable the "All files" option.
        chooser.setAcceptAllFileFilterUsed(false);
        //    
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            selectedDirectory2 = chooser.getSelectedFile();
            tfFolderDir2.setText(selectedDirectory2.getAbsolutePath());
        } else {
            System.out.println("No Selection Folder");
        }
    }//GEN-LAST:event_bBrowse6ActionPerformed

    private void bProcess2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bProcess2ActionPerformed
        // TODO add your handling code here:
        if ((selectedFile2 != null) && (selectedFile3 != null) && (selectedFile4 != null) && (selectedDirectory2 != null) && (knn != null)) {
            bProcess2.setEnabled(false);
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            task2 = new Task2() {
            };
            //task.addPropertyChangeListener(this);
            task2.execute();
        } else {
            JOptionPane.showMessageDialog(this, "Masukkan file terlebih dahulu");
        }
    }//GEN-LAST:event_bProcess2ActionPerformed

    private void bProcess3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bProcess3ActionPerformed
        // TODO add your handling code here:
        if (jComboBox1.getSelectedItem() != null && jComboBox2.getSelectedItem() != null) {
            //frameChart fc = new frameChart(bulan, provinsi);
            EventQueue.invokeLater(new Runnable() {
                public void run() {

                    try {
                        String bulan = (String) jComboBox1.getSelectedItem();
                        String provinsi = (String) jComboBox2.getSelectedItem();
                        provinsi = provinsi.toLowerCase();

                        frameChart frame = new frameChart(bulan, provinsi);
                        frame.setVisible(true);

                        String SQL = "SELECT day(tanggal),COUNT(id) as 'jumlah tweet'"
                                + " FROM TWEET"
                                + " WHERE label='flu' and lokasi='" + provinsi + "' and monthname(tanggal)='" + bulan + "' group by day(tanggal)";
                        JDBCCategoryDataset data = new JDBCCategoryDataset("jdbc:mysql://localhost/skripsi", "com.mysql.jdbc.Driver", "root", "");
                        data.executeQuery(SQL);
                        provinsi = provinsi.toUpperCase();
                        JFreeChart chart = ChartFactory.createLineChart("Visualisasi FLU Indonesia 2014 Bulan " + bulan + " Provinsi " + provinsi + "", "Tanggal", "Jumlah", data, PlotOrientation.VERTICAL, true, true, false);
                        ChartPanel cPanel = new ChartPanel(chart);
                        final CategoryPlot plot = chart.getCategoryPlot();
                        LineAndShapeRenderer renderer = new LineAndShapeRenderer();
                        plot.setRenderer(renderer);
                        frame.setContentPane(cPanel);
                        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }//GEN-LAST:event_bProcess3ActionPerformed


    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged

    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        //if (JOptionPane.showConfirmDialog(null, "Are you sure to close this window?", "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
            Statement stmt = null;
            try {
                koneksi();
                String query = "DROP TABLE IF EXISTS TWEET";
                stmt = conn.createStatement();
                stmt.executeUpdate(query);
            } catch (SQLException ex) {
                Logger.getLogger(Preprocessing.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.exit(0);
        //}
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Preprocessing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Preprocessing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Preprocessing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Preprocessing.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Preprocessing().setVisible(true);
                new Preprocessing().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bBrowse1;
    private javax.swing.JButton bBrowse2;
    private javax.swing.JButton bBrowse3;
    private javax.swing.JButton bBrowse4;
    private javax.swing.JButton bBrowse5;
    private javax.swing.JButton bBrowse6;
    private javax.swing.JButton bProcess1;
    private javax.swing.JButton bProcess2;
    private javax.swing.JButton bProcess3;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JFileChooser fileChooser2;
    private javax.swing.JFileChooser fileChooser3;
    private javax.swing.JFileChooser fileChooser4;
    private javax.swing.JFileChooser fileChooser5;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField knn;
    private javax.swing.JLabel lProgres;
    private javax.swing.JProgressBar pbLoading1;
    private javax.swing.JProgressBar pbLoading2;
    private javax.swing.JTextArea taKlasifikasi;
    private javax.swing.JTextArea taPrepro;
    private javax.swing.JTextField tfFileDir;
    private javax.swing.JTextField tfFileDir2;
    private javax.swing.JTextField tfFileDir3;
    private javax.swing.JTextField tfFileDir4;
    private javax.swing.JTextField tfFolderDir;
    private javax.swing.JTextField tfFolderDir2;
    private javax.swing.JTextField tfJumlahData;
    // End of variables declaration//GEN-END:variables

}
