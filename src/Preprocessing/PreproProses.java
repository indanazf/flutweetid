/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preprocessing;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Indana Zulfa
 */
public class PreproProses extends PreproAbstrak {

    String input = null;
    String output = null;
    String stopwords = null;
    String sinonim = null;
    String inputsin = null;
    String outputsin = null;
    private ArrayList<String> DictSW = new ArrayList<>();
    private HashMap<String, String> DictSY = new HashMap<>();

    public PreproProses(String i, String o, String sw, String sy, String os, String is) {
        this.input = i;
        this.output = o;
        this.stopwords = sw;
        this.sinonim = sy;
        this.outputsin = os;
        this.inputsin = is;
    }

    public static void main(String[] args) {
        String stopwords = "D:\\TWEET\\catatan_stopwords.txt";
        String sinonim = "D:\\TWEET\\catatan_kata_sinonim.txt";

        String input = "D:\\data tweet\\data train 1\\data_train_1.txt";
        String output = "D:\\data tweet\\data train 1\\data_train_prepro.txt";

        String inputsin = output;
        String outputsin = "D:\\data tweet\\data train 1\\data_train_syn.txt";

        PreproProses p = new PreproProses(input, output, stopwords, sinonim, outputsin, inputsin);
        try {
            p.process();
            p.processSW();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadSW() throws IOException {
        File sw = new File(stopwords);

        FileReader reader = new FileReader(sw);
        BufferedReader br = new BufferedReader(reader);

        String line = null;
        int count = 0;
        while ((line = br.readLine()) != null) {
            count++;
            DictSW.add(line);
        }

    }

    public void loadSin() throws IOException {
        File sin = new File(sinonim);
        FileReader reader = new FileReader(sin);
        BufferedReader br = new BufferedReader(reader);
        String line = null;
        int count = 0;
        String[] kata;
        while ((line = br.readLine()) != null) {
            count++;
            kata = line.split("=");
            DictSY.put(kata[0], kata[1]);
        }
    }

    public void processSW() throws IOException {
        loadSW();
        loadSin();

        File in = new File(inputsin);
        File out = new File(outputsin);

        FileReader reader = new FileReader(in);
        BufferedReader br = new BufferedReader(reader);
        FileWriter writer = new FileWriter(out);

        System.out.println("proses stopword & sinonim");

        String line = null;
        int count = 0;
        String kata1, kata2;
        while ((line = br.readLine()) != null) {
            count++;
            String[] tweet = line.split("[\\x7C][\\x7C]");
            Scanner scan = new Scanner(tweet[1]);
            StringBuilder sb = new StringBuilder();
            sb.append(tweet[0]).append("||");

            while (scan.hasNext()) {
                kata1 = scan.next();
                kata2 = DictSY.get(kata1);

                if (kata2 != null) {
                    kata1 = kata2;
                }
                if (!DictSW.contains(kata1)) {
                    sb.append(kata1).append(" ");
                }
            }
            sb.append("||").append(tweet[2]).append("||").append(tweet[3]).append("||").append(tweet[4]).append(" ");
            
            writer.write(sb.toString() + "\r\n");

        }
        br.close();
        writer.close();

        System.out.println("selesai");
    }

    public void process() throws IOException {
        File in = new File(input);
        File out = new File(output);

        FileReader reader = new FileReader(in);
        BufferedReader br = new BufferedReader(reader);
        FileWriter writer = new FileWriter(out);

        System.out.println("proses prepro");

        String line = null;
        while ((line = br.readLine()) != null) {
            int index = line.indexOf("||");

            if (line != null && index >= 0 && index <= line.length()) {

                //String tweet = line.substring(0, index);
                //String label = line.substring(index + 2);
                if (line.contains("||")) {
                    String[] parts = line.split("[\\x7C][\\x7C]");
                    String tweet = parts[0];
                    String tgl = parts[1];
                    String label = parts[3];
                    String lokasi = parts[2];
                    
                    
                    //tweet = replaceRT(tweet);
                    writer.write(tweet);
                    tweet = filter(tweet);
                    if ("".equals(tweet)) {
                    } else {
                        
                        writer.write("||" + tweet + "||" +tgl+"||"+  lokasi + "||" + label + "\r\n");
                        //writer.write(">>" + tweet + "||" +tgl+"||"+ label + "||"+lokasi+"\r\n");
                    }

                }
            }
        }

        br.close();
        writer.close();

        System.out.println("selesai prepro");
    }
    
    
    
}
