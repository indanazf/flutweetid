/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preprocessing;

import java.io.IOException;

/**
 *
 * @author Indana Zulfa
 */

public abstract class PreproAbstrak {

    public PreproAbstrak() {

    }

    public abstract void process() throws IOException;

    public String filter(String str) {
        str = str.toLowerCase();
        str = replaceURL(str);
        str = replaceHTMLent(str);
        str = replaceEmoticon(str);
        str = replaceUname(str);
        str = replaceHashtag(str);
        str = str.replaceAll("[^a-z0-9<>]"," ");
        str = deleteNonascii(str);

        return str;
    }

    protected String replaceHTMLent(String str) {
        String[] html = {"&ndash;", "&quot;", "&nbsp;", "&amp;", "&gt;",
            "&lt;"};
        String[] ascii = {"-", "\"", " ", "&", ">", "<"};

        for (int i=0; i<html.length; i++) {
            str = str.replace(html[i], ascii[i]);
        }

        return str;
    }
    
    protected String replaceEmoticon (String str){
        String[] emote = {":-&", ":&", ":-)", "o:)",":)", "=))",":'-(",":'(","{}",
            ":*",":(",":-(",":@","-_-","<3","</3",";-)",";)",":p",
             ":d",":v",":3","x_x",":o",">.<","t.t","u.u","t-t"
        };
        String[] ascii = {" <sakit> ", " <sakit> ", " <senyum> ", " <berdoa> ", " <senyum> "," <senyum> "," <menangis> "," <menangis> "," <peluk> ",
            " <cium> ", " <sedih> ", " <sedih> "," <marah> "," <gugup> "," <hati> "," <patahhati> "," <berkedip> "," <berkedip> "," <mengejek> ",
            " <tertawa> "," <pacman> "," <catface> "," <sakit> "," <terkejut> "," <skeptis> "," <menangis> "," <bingung> "," <menangis> "
        };

        
        for(int i=0; i<emote.length; i++){
            str=str.replace(emote[i],ascii[i]);
        }
        return str;
    }
    
    protected String replaceUname(String str){
        return str.replaceAll("@[\\S]+","");
    }
    
    protected String replaceHashtag(String str){
        return str.replaceAll("#[\\S]+","");
    }
    
    protected String replaceRT(String str) {
        return str.replaceAll("RT.*", "");
    }

    protected String replaceQuotes(String str) {
        return str.replaceAll("\".*", "");
    }

    protected String replaceURL(String s) {
        s = s.replaceAll("(?i)(?:https?|ftps?)://[\\w/%.-]+", "");
        s = s.replaceAll("(?i)\\b(\\S)*picture.twitter(\\S)*\\b", "");
        return s;
    }

    protected String deleteNonascii(String s) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c < 0x7F) {
                b.append(c);
            }
        }
        return b.toString();
    }
    
    
}
