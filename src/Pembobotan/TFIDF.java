/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pembobotan;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author indana zulfa
 */
public class TFIDF {

    String input = null;
    String output = null;
    HashMap<String, Integer> allKataCount = new HashMap<String, Integer>(); //kata dari semua dokumen

    Integer jumlahKataCount = 0;
    Integer jumlahDok = 0;

    public static void main(String[] args) {
        String input = "D:\\data tweet\\tes_syn.txt";
        String output = "D:\\data tweet\\tes_tfidf.txt";

        TFIDF p = new TFIDF();
        try {
            p.hitungKata(input);
            p.hitungTFIDF(input, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
   
    
    public void hitungKata(String filename) throws FileNotFoundException, IOException {
        System.out.println("proses TFIDF");

        FileReader reader = new FileReader(filename);
        BufferedReader br = new BufferedReader(reader);
        String line;

        while ((line = br.readLine()) != null) {

            String[] parts = line.split("[\\x7C][\\x7C]");
            String tweet = parts[1];
            String[] tokens = tweet.split(" ");

            for (String token : tokens) {
                if (token.isEmpty()) {
                    continue;
                }
                Integer count = 1;
                if (allKataCount.containsKey(token)) {
                    count += allKataCount.get(token).intValue();
                    jumlahKataCount--;
                }
                allKataCount.put(token, new Integer(count));
                jumlahKataCount++;
            }
            
            jumlahDok++;
        }
        br.close();
    }

    public void hitungTFIDF(String input, String output) throws FileNotFoundException, IOException {
        FileReader reader = new FileReader(input);
        BufferedReader br = new BufferedReader(reader);
        FileWriter writer = new FileWriter(output);
        StringBuilder sb = new StringBuilder();
        String line;

        while ((line = br.readLine()) != null) {

            String[] parts = line.split("[\\x7C][\\x7C]");
            String tweet = parts[1];
            String[] tokens = tweet.split(" ");
            sb.append(line).append("||");
            HashMap<String, Long> dokKataCount = new HashMap<String, Long>(); //kata per dok

            for (String token : tokens) {
                Integer count = 1;
                if (token.isEmpty()) {
                    continue;
                }
                if (dokKataCount.containsKey(token)) {
                    count += dokKataCount.get(token).intValue();
                }
                dokKataCount.put(token, new Long(count));
                //System.out.println(dokKataCount.keySet());
                
            }
            for (String token : dokKataCount.keySet()) {
                double dokKata = dokKataCount.get(token).doubleValue();
                double allKata = allKataCount.get(token).doubleValue();
                double tfidf = dokKata * Math.log10((double)jumlahDok/allKata);
                
                //System.out.println(dokKataCount.keySet());
                //System.out.print(String.format("%s=%f;", token, tfidf));
                sb.append(token).append("=").append(tfidf).append(";");
                //String key = token.toString();
                //String value = allKataCount.get(token).toString();
                //System.out.print(key + " " + value);
            }
            //System.out.println();
            sb.append("\r\n");

        }
        System.out.println("selesai TFIDF");
        writer.write(sb.toString() + "\r\n");

        br.close();
        writer.close();
    }

}
